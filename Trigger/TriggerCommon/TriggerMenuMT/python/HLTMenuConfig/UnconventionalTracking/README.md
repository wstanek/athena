Modules in this directory
-----

* [GenerateUnconventionalTrackingChainDefs](GenerateUnconventionalTrackingChainDefs.py)
  * Called by the menu code to pass the chain dict into the ChainConfiguration object
* [UnconventionalTrackingChainConfiguration](UnconventionalTrackingChainConfiguration.py)
  * Defines the ChainConfiguration object that interprets the chain dict and builds the chain